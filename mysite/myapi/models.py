from django.db import models


class Car(models.Model):
    make = models.CharField(max_length=60)
    model = models.CharField(max_length=60)
    
    #constraint unique together (make, model)

    def __str__(self):
        return '%s %s' %(self.make, self.model)

class Rating(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    rating = models.IntegerField()
    
