import requests



class NHTSA_VehicleAPI(object):
    root_url = 'https://vpic.nhtsa.dot.gov/api/vehicles'
    
    def __init__(self, root_url=None):
        if root_url is not None:
            self.root_url = root_url
        return
    
    def find_car(self, make=None, model=None):
        url = '%s/GetModelsForMake/%s?format=json' %(self.root_url, make)
        r = requests.get(url)
        if r.status_code == 200:
            for result in r.json()['Results']:
                if result['Model_Name'].lower() == model.lower():
                    return {'make': make.capitalize(), 'model': result['Model_Name']}
        return


            
    
    
