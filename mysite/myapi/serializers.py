from django.db.models import Avg, F, Count

from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from .vehicle_api import NHTSA_VehicleAPI


from .models import Car, Rating



class RatingSerializer(serializers.HyperlinkedModelSerializer):
    car_id = serializers.IntegerField(read_only=False, source="car.id")

    class Meta:
        model = Rating
        fields = ('car_id', 'rating')

    def validate(self, data):
        if data['rating'] <1 or data['rating']>5:
            raise serializers.ValidationError("rating must be an integer between 1 and 5")
        if not Car.objects.filter(id=data['car']['id']).exists():
            raise serializers.ValidationError("car not in database")

        return data

    def create(self, validated_data):
        return Rating.objects.create(car=Car.objects.filter(id=validated_data['car']['id']).first(),
                          rating=validated_data['rating'])
    
    


class CarExists(object):
    vehicle_api = NHTSA_VehicleAPI()

    def __call__(self, data):
        car = self.vehicle_api.find_car(make=data['make'], model=data['model'])
        if car is None:
            raise serializers.ValidationError("car does not exist")
        return car
        

class CarSerializer(serializers.HyperlinkedModelSerializer):
    #url = serializers.CharField(source='get_absolute_url', read_only=True)
    vehicle_api = NHTSA_VehicleAPI()
    avg_rating = serializers.SerializerMethodField()
    
    class Meta:
        model = Car
        fields = ('id', 'make', 'model', 'avg_rating')
        #read_only_fields = ['account_name']
        # avg_rating: 4.7

    def get_avg_rating(self, obj):
        # for particular musician get all albums and aggregate the all stars and return the avg_rating
        return obj.rating_set.aggregate(avgs=Avg(F('rating'))).get('avgs',None)        
    
    def validate(self, data):
        car = self.vehicle_api.find_car(make=data['make'], model=data['model'])
        if car is None:
            raise serializers.ValidationError("car does not exist")
        
        if Car.objects.filter(make=car['make'], model=car['model']).exists():
            raise serializers.ValidationError("car already in database")
        #should it return error or none
        return car
    
class PopularSerializer(serializers.HyperlinkedModelSerializer):
    rates_number = serializers.SerializerMethodField()

    class Meta:
        model = Car
        fields = ('id', 'make', 'model', 'rates_number')
        
    def get_rates_number(self, obj):
        # for particular musician get all albums and aggregate the all stars and return the avg_rating
        return obj.rating_set.aggregate(counts=Count(F('rating'))).get('counts',None)        
        

        
'''
class HighScoreSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        return {
            'score': instance.score,
            'player_name': instance.player_name
        }

@api_view(['GET'])
def all_high_scores(request):
    queryset = HighScore.objects.order_by('-score')
    serializer = HighScoreSerializer(queryset, many=True)
    return Response(serializer.data)

scores = serializers.ListField(
   child=serializers.IntegerField(min_value=0, max_value=100)
)


from rest_framework.reverse import reverse

class CustomerHyperlink(serializers.HyperlinkedRelatedField):
    # We define these as class attributes, so we don't need to pass them as arguments.
    view_name = 'customer-detail'
    queryset = Customer.objects.all()

    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            'organization_slug': obj.organization.slug,
            'customer_pk': obj.pk
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)

    def get_object(self, view_name, view_args, view_kwargs):
        lookup_kwargs = {
           'organization__slug': view_kwargs['organization_slug'],
           'pk': view_kwargs['customer_pk']
        }
        return self.get_queryset().get(**lookup_kwargs)


from rest_framework.reverse import reverse
from rest_framework.views import APIView
from django.utils.timezone import now

class APIRootView(APIView):
    def get(self, request):
        year = now().year
        data = {
            ...
            'year-summary-url': reverse('year-summary', args=[year], request=request)
        }
        return Response(data)

api_root = reverse_lazy('api-root', request=request)


from rest_framework import status
from rest_framework.response import Response

def empty_view(self):
    content = {'please move along': 'nothing to see here'}
    return Response(content, status=status.HTTP_404_NOT_FOUND)

    
from rest_framework import status
from rest_framework.test import APITestCase

class ExampleTestCase(APITestCase):
    def test_url_root(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
'''

# * Add a rate for a car from 1 to 5
# (maybe simpler to maintain rate count and average as fields)
# Model fields which have editable=False set, and AutoField fields will be set to read-only
# by default, and do not need to be added to the read_only_fields option.

'''
serializer = CommentSerializer(data={'email': 'foobar', 'content': 'baz'})
serializer.is_valid()
# False
serializer.errors
# {'email': ['Enter a valid e-mail address.'], 'created': ['This field is required.']}

# Return a 400 response if the data was invalid.
serializer.is_valid(raise_exception=True)


class BlogPostSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    content = serializers.CharField()
    # edits = EditItemSerializer(many=True)  # A nested list of 'edit' items.

    def validate_title(self, value):
        """
        Check that the blog post is about Django.
        """
        if 'django' not in value.lower():
            raise serializers.ValidationError("Blog post is not about Django")
        return value

# https://www.django-rest-framework.org/api-guide/serializers/

serializer.errors
# {'user': {'email': ['Enter a valid e-mail address.']}, 'created': ['This field is required.']}
'''

