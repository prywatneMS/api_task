from rest_framework import viewsets, status, mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Count

from .serializers import CarSerializer, RatingSerializer, PopularSerializer
from .models import Car, Rating
from rest_framework import status



class RatingViewSet(viewsets.ModelViewSet):
    queryset = Rating.objects.all()#.order_by('rate_set__avg')
    serializer_class = RatingSerializer

    def perform_create(self, serializer):
        
        serializer.save()


class CarViewSet(viewsets.ModelViewSet):
    queryset = Car.objects.all()#.order_by('rate_set__avg')
    serializer_class = CarSerializer
    
    def perform_create(self, serializer):
        # validate?
        #owner=self.request.user <- not
        if not serializer.is_valid():
            raise ValidationError('Make Model not found in vtcha API')        
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)

    def destroy(self, request, pk=None):
        instance = self.queryset.filter(id=pk).first()
        self.perform_destroy(instance)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()


class PopularListViewSet(mixins.ListModelMixin,
                         viewsets.GenericViewSet):
    serializer_class = PopularSerializer
    
    def get_queryset(self, ):
        queryset = Car.objects.all()#.order_by('rating_set__count')
        return queryset.annotate(popularity=Count('rating')).order_by('-popularity')
