from django.urls import include, path
from rest_framework import routers
from . import views



router = routers.DefaultRouter()
router.register(r'cars', views.CarViewSet)
router.register(r'popular', views.PopularListViewSet, basename='PopularList')
router.register(r'rate', views.RatingViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
