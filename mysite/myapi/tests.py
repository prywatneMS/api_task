from django.test import TestCase
from rest_framework.test import APIRequestFactory
#from coreapi import CoreApiClient

from django.urls import reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from .models import Car, Rating



class MockNHTSA_VehicleAPI(object):
    
    def __init__(self, ):
        return
    
    def find_car(self, make=None, model=None):
        existing_cars = {'Volkswagen': {'Passat', 'Golf'}, 'Fiat': {'Panda'}}
        existing_models = existing_cars.get(make)
        if models is None:
            return
        if model.capitalize() in existing_models:
           return {'make': make.capitalize(), 'model': model.capitalize()}


class CarTests(APITestCase):

    def test_create_car(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('car-list')
        data = {'make': 'Volkswagen', 'model': 'Golf'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Car.objects.count(), 1)
        self.assertEqual(Car.objects.get().make, 'Volkswagen')
        self.assertEqual(Car.objects.get().model, 'Golf')

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        
class RatingTests(APITestCase):
    
    def test_rate_car(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('car-list')
        data = {'make': 'Volkswagen', 'model': 'Golf'}
        response = self.client.post(url, data, format='json')
        id = Car.objects.get().id
        url = reverse('rating-list')
        data = {'car_id': id, 'rating': 5}

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Rating.objects.count(), 1)
        self.assertEqual(Rating.objects.get().car.id, id)
        self.assertEqual(Rating.objects.get().rating, 5)




# Create your tests here.
'''
5. At least basic tests of endpoints and their functionality are obligatory. Show us how good unit tests
should look like!
'''

'''
# Using the standard RequestFactory API to create a form POST request
factory = APIRequestFactory()
request = factory.post('/notes/', {'title': 'new idea'})
request = factory.post('/notes/', {'title': 'new idea'}, format='json')

# Fetch the API schema
client = CoreAPIClient()
schema = client.get('http://testserver/schema/')

# Create a new organisation
params = {'name': 'MegaCorp', 'status': 'active'}
client.action(schema, ['organisations', 'create'], params)

# Ensure that the organisation exists in the listing
data = client.action(schema, ['organisations', 'list'])
assert(len(data) == 1)
assert(data == [{'name': 'MegaCorp', 'status': 'active'}])
'''

'''
#pomyslec czy autoryzowac

from django.urls import include, path, reverse
from rest_framework.test import APITestCase, 
'''